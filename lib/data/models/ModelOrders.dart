class ModelOrder {
  final String id;
  final String pointAddress;
  final String pointCountry;
  final String pointPhone;
  final String? pointOthers;
  final String packageItem;
  final int packageWeight;
  final int packageWorth;
  final int status;

  ModelOrder({
    required this.id,
    required this.pointAddress,
    required this.pointCountry,
    required this.pointPhone,
    required this.pointOthers,
    required this.packageItem,
    required this.packageWeight,
    required this.packageWorth,
    required this.status});

  static ModelOrder fromJson(Map<String, dynamic> json) {
    return ModelOrder(
        id: json['id'],
        pointAddress: json['address'],
        pointCountry: json['country'],
        pointPhone: json['phone'],
        pointOthers: json['others'],
        packageItem: json['package_items'],
        packageWeight: json['weight_items'],
        packageWorth: json['worth_items'],
        status: json['status']
    );
  }
}