class ModelDestinations {
  final String id;
  final String destinationAddress;
  final String destinationCountry;
  final String destinationPhone;
  final String? destinationOthers;

  ModelDestinations({required this.id,required this.destinationAddress,
    required this.destinationCountry,
    required this.destinationPhone,
    required this.destinationOthers});

  static ModelDestinations fromJson(Map<String, dynamic> json) {
    return ModelDestinations(
      id: json['id'],
      destinationAddress: json['address'],
      destinationCountry: json['country'],
      destinationPhone: json['phone'],
      destinationOthers: json['others'],
    );
  }
}
