class ModelPoints{
  final String id;
  final String geoLat;
  final String geoLong;

  ModelPoints({required this.id, required this.geoLat, required this.geoLong});

  static ModelPoints fromJson(Map<String, dynamic> json){
    return ModelPoints(
        id: json['id'],
        geoLat: json['latitude'],
        geoLong: json['longitude']
    );
  }
}