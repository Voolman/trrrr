import 'package:intl/intl.dart';

class ModelTransaction {
  final String id;
  final String idUser;
  final String sum;
  final String title;
  final DateTime createdAt;

  ModelTransaction({required this.id,
    required this.idUser,
    required this.sum,
    required this.title,
    required this.createdAt});

  static ModelTransaction fromJson(Map<String, dynamic> json) {
    return ModelTransaction(
        id: json['id'],
        idUser: json['id_user'],
        sum: json['sum'],
        title: json['title'],
        createdAt: DateFormat("yyyy-MM-ddTHH:mm:ss").parse(json['created_at'])
    );
  }

  String formatDate() {
    return DateFormat("MMMM dd, yyyy")
        .format(createdAt)
        .replaceAll(".", "");
  }

}