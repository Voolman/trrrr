class ModelProfile {
  final String idUser;
  final String fullName;
  final String phone;
  final String? avatar;
  final int balance;
  final bool? raise;
  final String? createdAt;

  ModelProfile({required this.idUser,
    required this.fullName,
    required this.phone,
    required this.avatar,
    required this.balance,
    required this.raise,
    required this.createdAt});

  static ModelProfile fromJson(Map<String, dynamic> json) {
    return ModelProfile(
        idUser: json['id_user'],
        fullName: json['fullname'],
        phone: json['phone'],
        avatar: json['avatar'],
        balance: json['balance'],
        raise: json['raise'],
        createdAt: json['created_at']
    );
  }
}
