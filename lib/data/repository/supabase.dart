import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_7/data/models/ModelOrders.dart';
import 'package:training_final_7/data/models/ModelPoints.dart';

import '../models/ModelDestination.dart';
import '../models/ModelProfile.dart';
import '../models/ModelTransaction.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signUp(String email, String password) async {
  return supabase.auth.signUp(email: email, password: password);
}

Future<void> dataSignUp(String userId, String phone, String fullName) async {
  return supabase
      .from('profiles')
      .insert({'id_user': userId, 'fullname': fullName, 'phone': phone});
}

Future<AuthResponse> signIn(String email, String password) async {
  return supabase.auth.signInWithPassword(email: email, password: password);
}

Future<void> signOut() async {
  return supabase.auth.signOut();
}

Future<void> sendOTP(String email) async {
  return supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> verifyOTP(String email, String code) {
  return supabase.auth
      .verifyOTP(email: email, token: code, type: OtpType.email);
}

Future<UserResponse> changePassword(String password) {
  return supabase.auth.updateUser(UserAttributes(password: password));
}

Future<List<ModelProfile>> getUserData() async {
  var response = await supabase.from('profiles').select();
  return response.map((e) => ModelProfile.fromJson(e)).toList();
}

String? getCurrentUserId() {
  var res = supabase.auth.currentUser?.id;
  return res;
}

Future<void> saveOrigin(
  String addressOrder,
  String countryOrder,
  String phoneOrder,
  String othersOrder,
  String items,
  String weight,
  String worth,
  int charges,
  int instant,
) async {
  var currentId = getCurrentUserId();
  await supabase.from('orders').insert({
    'id_user': currentId,
    'address': addressOrder,
    'country': countryOrder,
    'phone': phoneOrder,
    'others': othersOrder,
    'package_items': items,
    'weight_items': weight,
    'worth_items': worth,
    'delivery_charges': charges,
    'instant_delivery': instant,
    'tax_and_service_charges': (charges + instant) * 0.05,
    'sum_price': charges + instant + (charges + instant) * 0.05
  });
}

Future<void> saveDestinations(
  List<TextEditingController> destinationAddress,
  List<TextEditingController> destinationCountry,
  List<TextEditingController> destinationPhone,
  List<TextEditingController> destinationOthers,
) async {
  var currentUserId = getCurrentUserId();
  var currentOrder =
      await supabase.from('orders').select().eq('id_user', currentUserId!);
  var currentId = currentOrder
      .map((e) => ModelOrder.fromJson(e))
      .toList()[currentOrder.length - 1];
  for (int i = 0; i < destinationAddress.length; i++) {
    await supabase.from('destinations_details').insert({
      'id_order': currentId.id,
      'address': destinationAddress[i].text,
      'country': destinationCountry[i].text,
      'phone': destinationPhone[i].text,
      'others': destinationOthers[i].text,
    });
  }
}

Future<List<ModelTransaction>> getTransactions() async {
  var currentId = getCurrentUserId();
  var response =
      await supabase.from('transactions').select().eq('id_user', currentId!);
  return response.map((e) => ModelTransaction.fromJson(e)).toList();
}

Future<String> getOrderId() async {
  var currentUserId = getCurrentUserId();
  var currentOrder =
      await supabase.from('orders').select().eq('id_user', currentUserId!);
  var currentId = currentOrder
      .map((e) => ModelOrder.fromJson(e))
      .toList()[currentOrder.length - 1];
  return currentId.id;
}

Future<ModelOrder> getOrderInformation() async {
  var currentUserId = getCurrentUserId();
  var response =
      await supabase.from('orders').select().eq('id_user', currentUserId!);
  return response
      .map((e) => ModelOrder.fromJson(e))
      .toList()[response.length - 1];
}

Future<List<ModelDestinations>> getDestinationInformation() async {
  var currentId = await getOrderId();
  var response = await supabase
      .from('destinations_details')
      .select()
      .eq('id_order', currentId);
  return response.map((e) => ModelDestinations.fromJson(e)).toList();
}

Future<void> pushRate(int rate, String feedback) async {
  var currentId = await getOrderId();
  return await supabase
      .from('orders')
      .update({'rate': rate, 'feedback': feedback}).eq('id', currentId);
}

Future<void> subscribeOrder(String idOrder, Function callbackFunc) async {
  supabase
      .channel('update-status-order')
      .onPostgresChanges(
          event: PostgresChangeEvent.update,
          schema: 'public',
          table: 'orders',
          filter:
              PostgresChangeFilter(
                  type: PostgresChangeFilterType.eq,
                  column: 'id',
                  value: idOrder
              ),
          callback: (payload){
            callbackFunc(payload.newRecord);
          }
  ).subscribe();
}

Future<List<ModelPoints>> getPoints() async {
  var response = await supabase.from('points').select();
  return response.map((e) => ModelPoints.fromJson(e)).toList();
}

Future<bool> checkPackage() async {
  var currentUserId = getCurrentUserId();
  var response =
      await supabase.from('orders').select().eq('id_user', currentUserId!);
  var res = response
      .map((e) => ModelOrder.fromJson(e))
      .toList();
  return res.isNotEmpty;
}
