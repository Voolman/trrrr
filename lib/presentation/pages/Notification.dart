import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../main.dart';

class Notifications extends StatefulWidget {
  const Notifications({super.key});


  @override
  State<Notifications> createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Column(
        children: [
          Container(
            width: double.infinity,
            height: 108,
            decoration: BoxDecoration(
              color: colors.background,
              boxShadow: const [
                BoxShadow(
                  color: Color.fromARGB(38, 0, 0, 0),
                  offset: Offset(0, 2),
                  blurRadius: 5
                )
              ]),
            child: Padding(
              padding: const EdgeInsets.only(top: 69, left: 15),
              child: Row(
                children: [
                  GestureDetector(
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                      child: SvgPicture.asset('assets/back.svg')),
                  const SizedBox(width: 114,),
                  Text(
                    'Notifications',
                    style: Theme.of(context).textTheme.labelSmall,
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 120),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 83,
                  width: 83,
                  child: SvgPicture.asset('assets/notification.svg', color: colors.subtext,)
                ),
                const SizedBox(height: 18),
                Text(
                  'You have no notifications',
                  style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.text),
                )
              ],
            ),
          )

        ],
      ),
    );
  }
}