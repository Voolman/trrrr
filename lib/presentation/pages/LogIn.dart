import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_7/domain/utils.dart';


import '../../domain/LogInPresenter.dart';
import '../theme/colors.dart';
import '../widgets/CustomTextField.dart';
import '../widgets/dialogs.dart';
import 'ForgotPassword.dart';
import 'Home.dart';
import 'SignUp.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});


  @override
  State<LogIn> createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  bool enableButton = false;
  bool isChecked = false;
  void onChanged(_){
    setState(() {
      enableButton = isValidLogIn(email.text, password.text);
    });
  }
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 155, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'Welcome Back',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Fill in your email and password to continue',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              CustomTextField(
                  label: 'Email Address',
                  hint: '***********@mail.com',
                  onChanged: onChanged,
                  controller: email,
                  isError: isValidEmail(email.text),
              ),
              CustomTextField(
                  label: 'Password',
                  hint: '**********',
                  enableObscure: true,
                  onChanged: onChanged,
                  controller: password,
                  isError: isValidFirstPassword(password.text),
              ),
              const SizedBox(height: 17),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: 21,
                        width: 21,
                        child: Checkbox(
                            value: isChecked,
                            activeColor: colors.primary,
                            side: BorderSide(color: colors.subtext, width: 1),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2)
                            ),
                            onChanged: (value){
                              setState(() {
                                isChecked = value!;
                              });
                            }
                        ),
                      ),
                      const SizedBox(width: 4),
                      Text(
                        'Remember password',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const ForgotPassword()));
                    },
                    child: Text(
                      'Forgot Password?',
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.primary, fontSize: 12),
                    ),
                  )
                ],
              ),
              const SizedBox(height: 187),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (enableButton) ? (){
                        showLoading(context);
                        pressSignIn(
                            email.text,
                            password.text,
                              (AuthResponse res){
                              (isChecked) ? saver(res.user!.id, email.text, password.text) : null;
                              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const Home()), (route) => false);},
                              (String e){showError(context, e);}
                        );
                        Navigator.of(context).pop();
                      } : null,
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Log in',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  )
              ),
              const SizedBox(height: 20),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const SignUp()));
                },
                child: RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Already have an account?',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                          ),
                          TextSpan(
                              text: 'Sign up',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.primary)
                          )
                        ]
                    )
                ),
              ),
              const SizedBox(height: 18),
              Text(
                'or sign in using',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
              ),
              const SizedBox(height: 8),
              Image.asset('assets/google.png')
            ],
          ),
        ),
      ),
    );
  }
}