import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../domain/SendPackagePresenter.dart';
import '../../main.dart';
import '../widgets/dialogs.dart';
import 'Transaction.dart';

class SendPackage2 extends StatefulWidget {
  final String originAddress;
  final String originCountry;
  final String originPhone;
  final String originOthers;
  final List<TextEditingController> destinationAddress;
  final List<TextEditingController> destinationCountry;
  final List<TextEditingController> destinationPhone;
  final List<TextEditingController> destinationOthers;
  final String packageItems;
  final String packageWeight;
  final String packageWorth;

  const SendPackage2(
      {super.key, required this.originAddress, required this.originCountry, required this.originPhone, required this.originOthers, required this.destinationAddress, required this.destinationCountry, required this.destinationPhone, required this.destinationOthers, required this.packageItems, required this.packageWeight, required this.packageWorth});

  @override
  State<SendPackage2> createState() => _SendPackage2State();
}
var id = '';
class _SendPackage2State extends State<SendPackage2> {
  @override
  void initState(){
    super.initState();
    id = getUserIdFunc()!;
  }
  List<Widget> destinations = [];

  List<Widget> createWidgets(){
    for(int i=0; i<widget.destinationAddress.length; i++){
      destinations.insert(i, Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 8),
          Text(
              '${i+1}. ${widget.destinationAddress[i].text} , ${widget
                  .destinationCountry[i].text}',
              style: Theme
                  .of(context)
                  .textTheme
                  .titleSmall
          ),
          const SizedBox(height: 4,),
          Text(
              widget.destinationPhone[i].text,
              style: Theme
                  .of(context)
                  .textTheme
                  .titleSmall
          ),
          (widget.destinationOthers.isNotEmpty) ? Text(
              widget.destinationOthers[i].text,
              style: Theme
                  .of(context)
                  .textTheme
                  .titleSmall
          ) : const SizedBox(),
        ],
        )
      );
    }
    return destinations;
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
        backgroundColor: colors.background,
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 108,
                width: double.infinity,
                child: Column(
                  children: [
                    SizedBox(
                      height: 106,
                      width: double.infinity,
                      child: Column(
                        children: [
                          const SizedBox(height: 73,),
                          Row(
                            children: [
                              const SizedBox(width: 15),
                              GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                  },
                                  child: SvgPicture.asset('assets/back.svg')
                              ),
                              const SizedBox(width: 99),
                              Text(
                                'Send a package',
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .titleMedium
                                    ?.copyWith(fontSize: 16),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 2,
                      decoration: const BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(38, 0, 0, 0),
                              blurRadius: 2,
                              offset: Offset(0, 2),
                            )
                          ]
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 24),
                    Text(
                      'Package Information',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: colors.primary
                      ),
                    ),
                    const SizedBox(height: 8,),
                    Text(
                      'Origin details',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                          color: colors.text
                      ),
                    ),
                    const SizedBox(height: 4,),
                    Text(
                        '${widget.originAddress} , ${widget.originCountry}',
                        style: Theme
                            .of(context)
                            .textTheme
                            .titleSmall
                    ),
                    const SizedBox(height: 4,),
                    Text(
                        widget.originPhone,
                        style: Theme
                            .of(context)
                            .textTheme
                            .titleSmall
                    ),
                    (widget.originOthers.isNotEmpty) ? Text(
                        widget.originOthers,
                        style: Theme
                            .of(context)
                            .textTheme
                            .titleSmall
                    ) : const SizedBox(),
                    const SizedBox(height: 8,),
                    Text(
                        'Destination details',
                        style: Theme
                            .of(context)
                            .textTheme
                            .titleSmall
                            ?.copyWith(color: colors.text)
                    ),
                    const SizedBox(height: 4,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[] + createWidgets(),
                    ),
                    const SizedBox(height: 8,),
                    Text(
                      'Other details',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                          color: colors.text
                      ),
                    ),
                    const SizedBox(height: 4,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            'Package Items',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                        ),
                        Text(
                            widget.packageItems,
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                                ?.copyWith(color: colors.secondary)
                        ),
                      ],
                    ),
                    const SizedBox(height: 4,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            'Weight of items',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                        ),
                        Text(
                            widget.packageWeight,
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                                ?.copyWith(color: colors.secondary)
                        ),
                      ],
                    ),
                    const SizedBox(height: 4,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            'Worth of Items',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                        ),
                        Text(
                            widget.packageWorth,
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                                ?.copyWith(color: colors.secondary)
                        ),
                      ],
                    ),
                    const SizedBox(height: 4,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            'Tracking Number',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                        ),
                        Text(
                            'R-$id',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                                ?.copyWith(color: colors.secondary)
                        ),
                      ],
                    ),
                    const SizedBox(height: 37,),
                    Divider(height: 1, color: colors.subtext),
                    const SizedBox(height: 8,),
                    Text(
                      'Charges',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: colors.primary
                      ),
                    ),
                    const SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            'Delivery Charges',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                        ),
                        Text(
                            'N${costCharges(widget.destinationAddress.length)}.00',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                                ?.copyWith(color: colors.secondary)
                        ),
                      ],
                    ),
                    const SizedBox(height: 8,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            'Instant delivery',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                        ),
                        Text(
                            'N300.00',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                                ?.copyWith(color: colors.secondary)
                        ),
                      ],
                    ),
                    const SizedBox(height: 8,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            'Tax and Service Charges',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                        ),
                        Text(
                            'N${costTax(widget.destinationAddress.length)}0',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                                ?.copyWith(color: colors.secondary)
                        ),
                      ],
                    ),
                    const SizedBox(height: 9,),
                    Divider(height: 1, color: colors.subtext),
                    const SizedBox(height: 4,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            'Package total',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                        ),
                        Text(
                            'N${totalCost(widget.destinationAddress.length).toString()}0',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleSmall
                                ?.copyWith(color: colors.secondary)
                        ),
                      ],
                    ),
                    const SizedBox(height: 46),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          height: 48,
                          width: 158,
                          child: OutlinedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              style: OutlinedButton.styleFrom(
                                  side: BorderSide(color: colors.primary),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  )
                              ),
                              child: Text(
                                'Edit package',
                                style: TextStyle(
                                    color: colors.primary,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700
                                ),
                              )
                          ),
                        ),
                        SizedBox(
                          height: 48,
                          width: 158,
                          child: FilledButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => Transaction(id: id)));
                              },
                              style: FilledButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                backgroundColor: colors.primary,
                              ),
                              child: Text(
                                'Make payment',
                                style: TextStyle(
                                    color: colors.background,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700
                                ),
                              )
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        )
    );
  }
}