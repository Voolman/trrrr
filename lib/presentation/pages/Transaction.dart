import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_7/domain/TransactionPresenter.dart';
import '../../main.dart';
import 'Home.dart';

class Transaction extends StatefulWidget {
  final String id;
  const Transaction({super.key, required this.id});

  @override
  State<Transaction> createState() => _TransactionState();
}

class _TransactionState extends State<Transaction> {
  @override
  void initState(){
    super.initState();
    timerTransaction(
      (){
        setState(() {});
      }
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 144, ),
            child: (getTime() == 0) ? Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset('assets/good.svg'),
                const SizedBox(height: 75,),
                Row(
                  children: [
                    Text(
                      'Transaction Successful',
                      style: Theme.of(context).textTheme.titleLarge,
                    )
                  ],
                )
              ],
            ) : Column(
              children: [
                Expanded(
                  child: CircularProgressIndicator(
                    backgroundColor: Colors.transparent,
                    valueColor: AlwaysStoppedAnimation<Color>(colors.secondary),
                    strokeWidth: 13.88,
                    strokeCap: StrokeCap.round,
                  ),
                ),
                const SizedBox(height: 130,),
              ],
            ),
          ),
          Text(
            'Your rider is on the way to your destination',
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: colors.text
            ),
          ),
          const SizedBox(height: 8,),
          RichText(text: TextSpan(
              children: [
                TextSpan(
                  text: 'Tracking Number ',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: colors.text
                  ),
                ),
                TextSpan(
                  text: 'R-${widget.id}',
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: colors.primary
                  ),
                )
              ]
          )),
          Padding(
            padding: const EdgeInsets.only(top: 141, left: 24, right: 24),
            child: Column(
              children: [
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home(currentIndex: 2)));
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                          'Track my item',
                          style: Theme.of(context).textTheme.labelMedium
                      )
                  ),
                ),
                const SizedBox(height: 8,),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: OutlinedButton(
                      onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home()));
                      },
                      style: OutlinedButton.styleFrom(
                          side: BorderSide(color: colors.primary),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4)
                          )
                      ),
                      child: Text(
                        'Go back to homepage',
                        style: TextStyle(
                            color: colors.primary,
                            fontSize: 16,
                            fontWeight: FontWeight.w700
                        ),
                      )
                  ),
                ),
              ],
            ),
          )

        ],
      ),
    );
  }
}
