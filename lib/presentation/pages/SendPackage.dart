import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:training_final_7/main.dart';
import 'package:training_final_7/presentation/widgets/CustomTextFieldPage.dart';

import '../../domain/SendPackagePresenter.dart';
import '../widgets/dialogs.dart';
import 'SendPackage2.dart';

class SendPackage extends StatefulWidget {
  const SendPackage({super.key});


  @override
  State<SendPackage> createState() => _SendPackageState();
}

class _SendPackageState extends State<SendPackage> {
  TextEditingController originAddress = TextEditingController();
  TextEditingController originCountry = TextEditingController();
  TextEditingController originPhone = TextEditingController();
  TextEditingController originOthers = TextEditingController();
  TextEditingController packageItems = TextEditingController();
  TextEditingController packageWorth = TextEditingController();
  TextEditingController packageWeight = TextEditingController();
  int destinations = 1;
  List<Widget> destinationsWidgets = [];
  List<TextEditingController> destinationAddress = [];
  List<TextEditingController> destinationCountry = [];
  List<TextEditingController> destinationPhone = [];
  List<TextEditingController> destinationOthers = [];


  List<Widget> createWidgets(int num){
      var colors = MyApp.of(context).getColors(context);
      destinationAddress.add(TextEditingController());
      destinationCountry.add(TextEditingController());
      destinationPhone.add(TextEditingController());
      destinationOthers.add(TextEditingController());
      var currentIndex = destinations - 1;
      destinationsWidgets.add(Column(
            children: [
              const SizedBox(height: 24,),
              Row(
                children: [
                  SvgPicture.asset('assets/point2.svg'),
                  const SizedBox(width: 8),
                  Text(
                    'Destination Details',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        height: 8/7,
                        color: colors.text
                    ),
                  )
                ],
              ),
              CustomTextFieldPage(
                  hint: 'Address',
                  controller: destinationAddress[currentIndex]
              ),
              CustomTextFieldPage(
                  hint: 'State,Country',
                  controller: destinationCountry[currentIndex]
              ),
              CustomTextFieldPage(
                  hint: 'Phone number',
                  controller: destinationPhone[currentIndex]
              ),
              CustomTextFieldPage(
                  hint: 'Others',
                  controller: destinationOthers[currentIndex]
              ),
            ],
          )
      );
    return destinationsWidgets;
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: ListView(
        children: [
          Column(
            children: [
                Container(
                width: double.infinity,
                height: 108,
                decoration: BoxDecoration(
                    color: colors.background,
                    boxShadow: const [
                      BoxShadow(
                          color: Color.fromARGB(38, 0, 0, 0),
                          offset: Offset(0, 2),
                          blurRadius: 5
                      )
                    ]),
                child: Padding(
                  padding: const EdgeInsets.only(top: 69, left: 15),
                  child: Row(
                    children: [
                      GestureDetector(
                          onTap: (){
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset('assets/back.svg')),
                      const SizedBox(width: 114,),
                      Text(
                        'Send a package',
                        style: Theme.of(context).textTheme.labelSmall,
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 43, left: 24, right: 24),
                child: Column(
                  children: [
                    Row(
                      children: [
                        SvgPicture.asset('assets/point.svg'),
                        const SizedBox(width: 8),
                        Text(
                          'Origin Details',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            height: 8/7,
                            color: colors.text
                          ),
                        )
                      ],
                    ),
                    CustomTextFieldPage(
                        hint: 'Address',
                        controller: originAddress
                    ),
                    CustomTextFieldPage(
                        hint: 'State,Country',
                        controller: originCountry
                    ),
                    CustomTextFieldPage(
                        hint: 'Phone number',
                        controller: originPhone
                    ),
                    CustomTextFieldPage(
                        hint: 'Others',
                        controller: originOthers
                    ),
                    Column(
                      children: <Widget>[] + createWidgets(destinations),
                    ),
                    const SizedBox(height: 14),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              destinations += 1;
                            });
                          },
                          child: SvgPicture.asset('assets/add.svg')
                        ),
                        const SizedBox(width: 8),
                        Text(
                          'Add destination',
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              height: 4/3,
                              color: colors.subtext
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 13),
                    Row(
                      children: [
                        Text(
                          'Package Details',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              height: 8/7,
                              color: colors.text
                          ),
                        )
                      ],
                    ),
                    CustomTextFieldPage(
                        hint: 'package items',
                        controller: packageItems
                    ),
                    CustomTextFieldPage(
                        hint: 'Weight of item(kg)',
                        controller: packageWeight
                    ),
                    CustomTextFieldPage(
                        hint: 'Worth of Items',
                        controller: packageWorth
                    ),
                    const SizedBox(height: 39),
                    Row(
                      children: [
                        Text(
                          'Select delivery type',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              height: 8/7,
                              color: colors.text
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 16),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 159,
                          height: 75,
                          decoration: const BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromARGB(38, 0, 0, 0),
                                  blurRadius: 2,
                                  offset: Offset(0, 2),
                                )]
                          ),
                          child: Expanded(
                              child: FilledButton(
                                  style: FilledButton.styleFrom(
                                      backgroundColor: colors.primary,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(8)
                                      )
                                  ),
                                  onPressed: (){
                                    pressSendPackage(
                                      originAddress.text,
                                      originCountry.text,
                                      originPhone.text,
                                      originOthers.text,
                                      packageItems.text,
                                      packageWeight.text,
                                      packageWorth.text,
                                      costCharges(destinationAddress.length),
                                      300,
                                      destinationAddress,
                                      destinationCountry,
                                      destinationPhone,
                                      destinationOthers,
                                          (){
                                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => SendPackage2(
                                              originAddress: originAddress.text,
                                              originCountry: originCountry.text,
                                              originPhone: originPhone.text,
                                              originOthers: originCountry.text,
                                              destinationAddress: destinationAddress,
                                              destinationCountry: destinationCountry,
                                              destinationPhone: destinationPhone,
                                              destinationOthers: destinationOthers,
                                              packageItems: packageItems.text,
                                              packageWeight: packageWeight.text,
                                              packageWorth: packageWorth.text,
                                            )
                                            )
                                            );
                                      },
                                          (String e){showError(context, e);}
                                    );
                                  },
                                  child: Column(
                                    children: [
                                      const SizedBox(height: 13,),
                                      SvgPicture.asset('assets/clock.svg'),
                                      const SizedBox(height: 10),
                                      const Text(
                                        'Instant delivery',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14
                                        ),
                                      )
                                    ],
                                  )
                              )
                          ),
                        ),
                        const SizedBox(width: 24),
                        Container(
                          width: 159,
                          height: 75,
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromARGB(38, 0, 0, 0),
                                  blurRadius: 2,
                                  offset: Offset(0, 2),
                                )]
                          ),
                          child: OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                  side: const BorderSide(color: Colors.white),
                                  backgroundColor: colors.background,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  )
                              ),
                              onPressed: (){},
                              child: Column(
                                children: [
                                  const SizedBox(height: 13,),
                                  SvgPicture.asset('assets/num.svg'),
                                  const SizedBox(height: 10),
                                  Text(
                                    'Scheduled delivery',
                                    style: TextStyle(
                                        color: colors.subtext,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12
                                    ),
                                  )
                                ],
                              )
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}