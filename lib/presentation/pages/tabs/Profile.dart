import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_7/presentation/pages/SendPackage.dart';



import '../../../data/models/ModelProfile.dart';
import '../../../domain/ProfilePresenter.dart';
import '../../../main.dart';
import '../../widgets/CustomListTile.dart';
import '../../widgets/dialogs.dart';
import '../AddPayment.dart';
import '../LogIn.dart';
import '../Notification.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}
bool isSee = true;
String name = '';
int balance = 0;
class _ProfileState extends State<Profile> {
  @override
  void initState(){
    super.initState();
    getProfile(
          (ModelProfile res){
          setState(() {
            name = res.fullName;
            balance = res.balance;
          });
        },
            (String e){showError(context, e);}
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Column(
        children: [
          Container(
            width: double.infinity,
            height: 108,
            decoration: BoxDecoration(color: colors.background, boxShadow: const [
              BoxShadow(
                  color: Color.fromARGB(38, 0, 0, 0),
                  offset: Offset(0, 2),
                  blurRadius: 5)
            ]),
            child: Padding(
              padding: const EdgeInsets.only(top: 73),
              child: Center(
                child: Column(
                  children: [
                    Center(
                      child: Text(
                        'Profile',
                        style: Theme.of(context).textTheme.labelSmall,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 27, left: 10, right: 19),
            child: SizedBox(
              height: 75,
              child: ListTile(
                contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                leading: Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                      color: colors.hint,
                      shape: BoxShape.circle,
                      border: Border.all(color: colors.hint, width: 1)
                  ),
                  child: const Icon(Icons.person),
                ),
                title: Text(
                  'Hello $name',
                  style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 16, height: 15/8),
                ),
                subtitle: RichText(text: TextSpan(
                    children: [
                      TextSpan(
                          text: 'Current balance: ',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: colors.text,
                              height: 4/3
                          )
                      ),
                      TextSpan(
                          text: (isSee) ? 'N$balance:00' : '*****',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: colors.primary,
                              height: 4/3
                          )
                      )
                    ]
                )),
                trailing: GestureDetector(
                  onTap: (){
                    setState(() {
                      isSee = !isSee;
                    });
                  },
                  child: SvgPicture.asset('assets/eye-slash.svg', colorFilter: ColorFilter.mode(colors.text, BlendMode.dst)),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 24, right: 24, top: 19),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Enable dark Mode',
                      style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.text),
                    ),
                    Switch(
                        inactiveThumbColor: Colors.white,
                        inactiveTrackColor: const Color.fromARGB(255, 215, 215, 215),
                        trackOutlineColor: MaterialStateProperty.resolveWith((states) => colors.background),
                        value: !MyApp.of(context).isLight,
                        onChanged: (value){
                          setState(() {
                            MyApp.of(context).changeColorsApp(context);
                          });
                        }
                    )
                  ],
                ),
                CustomListTile(
                    icon: SvgPicture.asset('assets/profile.svg', color: colors.text,),
                    label: "Edit Profile",
                    text: 'Name, phone no, address, email ...'
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SendPackage()));
                  },
                  child: CustomListTile(
                      icon: SvgPicture.asset('assets/paper.svg', colorFilter: ColorFilter.mode(colors.text, BlendMode.dst)),
                      label: "Statements & Reports",
                      text: 'Download transaction details, orders, deliveries'
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Notifications()));
                  },
                  child: CustomListTile(
                      icon: SvgPicture.asset('assets/notification.svg', color: colors.text,),
                      label: "Notification Settings",
                      text: 'mute, unmute, set location & tracking setting'
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const AddPayment()));
                  },
                  child: CustomListTile(
                      icon: SvgPicture.asset('assets/wallet.svg', color: colors.text,),
                      label: "Card & Bank account settings",
                      text: 'change cards, delete card details'
                  ),
                ),
                CustomListTile(
                    icon: SvgPicture.asset('assets/person.svg', colorFilter: ColorFilter.mode(colors.text, BlendMode.dst)),
                    label: "Referrals",
                    text: 'check no of friends and earn'
                ),
                CustomListTile(
                    icon: SvgPicture.asset('assets/map.svg', colorFilter: ColorFilter.mode(colors.text, BlendMode.dst)),
                    label: "About Us",
                    text: 'know more about us, terms and conditions'
                ),
                GestureDetector(
                  onTap: (){
                    showLoading(context);
                    pressSignOut(
                            (){
                          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const LogIn()), (route) => false);
                        },
                            (String e){showError(context, e);}
                    );
                    Navigator.of(context).pop();
                  },
                  child: CustomListTile(
                    icon: SvgPicture.asset('assets/log_out.svg', colorFilter: ColorFilter.mode(colors.error, BlendMode.dst)),
                    label: "Log Out",
                  ),
                ),
              ],
            ),
          )

        ],
      ),
    );
  }
}
