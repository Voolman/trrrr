import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:training_final_7/main.dart';

import '../../../data/models/ModelProfile.dart';
import '../../../data/models/ModelTransaction.dart';
import '../../../domain/ProfilePresenter.dart';
import '../../../domain/WalletPresenter.dart';
import '../../widgets/dialogs.dart';
import '../AddPayment.dart';

class Wallet extends StatefulWidget {
  const Wallet({super.key});


  @override
  State<Wallet> createState() => _WalletState();
}
bool isSee = true;
String name = '';
int balance = 0;
List<ModelTransaction> transactions = [];
class _WalletState extends State<Wallet> {
  @override
  void initState(){
    super.initState();
    getProfile(
            (ModelProfile res){
          setState(() {
            name = res.fullName;
            balance = res.balance;
          });
        },
            (String e){showError(context, e);}
    );
    getUserTransactions(
            (res){
          transactions = res;
        },
        (String e){showError(context, e);}
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Column(
        children: [
          Container(
            width: double.infinity,
            height: 108,
            decoration: BoxDecoration(color: colors.background, boxShadow: const [
              BoxShadow(
                  color: Color.fromARGB(38, 0, 0, 0),
                  offset: Offset(0, 2),
                  blurRadius: 5)
            ]),
            child: Padding(
              padding: const EdgeInsets.only(top: 73),
              child: Center(
                child: Column(
                  children: [
                    Center(
                      child: Text(
                        'Wallet',
                        style: Theme.of(context).textTheme.labelSmall,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 41, left: 15, right: 24),
            child: SizedBox(
              height: 75,
              child: ListTile(
                contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                leading: Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                      color: colors.hint,
                      shape: BoxShape.circle,
                      border: Border.all(color: colors.hint, width: 1)
                  ),
                  child: const Icon(Icons.person),
                ),
                title: Text(
                  'Hello $name',
                  style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 16, height: 15/8),
                ),
                subtitle: RichText(text: TextSpan(
                    children: [
                      TextSpan(
                          text: 'Current balance: ',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: colors.text,
                              height: 4/3
                          )
                      ),
                      TextSpan(
                          text: (isSee) ? 'N$balance:00' : '*****',
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: colors.primary,
                              height: 4/3
                          )
                      )
                    ]
                )),
                trailing: GestureDetector(
                  onTap: (){
                    setState(() {
                      isSee = !isSee;
                    });
                  },
                  child: SvgPicture.asset('assets/eye-slash.svg', colorFilter: ColorFilter.mode(colors.text, BlendMode.dst)),
                ),
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 28, left: 24, right: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: double.infinity,
                  height: 116,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: colors.hint
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 10),
                      Text(
                        'Top Up',
                        style: Theme.of(context).textTheme.labelMedium?.copyWith(color: colors.text),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12, left: 48, right: 48),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Container(
                                  height: 48,
                                  width: 48,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(360),
                                      color: colors.primary
                                  ),
                                  child: SvgPicture.asset('assets/bank.svg', fit: BoxFit.scaleDown)
                                ),
                                const SizedBox(height: 4,),
                                Text(
                                  'Bank',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Container(
                                  height: 48,
                                  width: 48,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(360),
                                      color: colors.primary
                                  ),
                                  child: SvgPicture.asset('assets/transfer.svg', fit: BoxFit.scaleDown)
                                ),
                                const SizedBox(height: 4,),
                                Text(
                                  'Transfer',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: (){
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const AddPayment()));
                                  },
                                  child: Container(
                                    height: 48,
                                    width: 48,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(360),
                                        color: colors.primary
                                    ),
                                    child: SvgPicture.asset('assets/card.svg', fit: BoxFit.scaleDown,)
                                  ),
                                ),
                                const SizedBox(height: 4,),
                                Text(
                                  'Card',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 41),
                Text(
                  'Transaction History',
                  style: TextStyle(
                    fontSize: 20,
                    height: 1,
                    fontWeight: FontWeight.w500,
                    color: colors.text
                  ),
                ),

                SizedBox(
                  height: 390,
                  width: double.infinity,
                  child: ListView.builder(
                      itemCount: transactions.length,
                      itemBuilder: (_, index){
                        return Column(
                          children: [
                            Container(
                              height: 44,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: colors.background,
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color.fromARGB(38, 0, 0, 0),
                                      blurRadius: 2,
                                      offset: Offset(0, 2),
                                    )]
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 12),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          transactions[index].sum,
                                          style: (transactions[index].sum[0] == '-') ? TextStyle(
                                              color: colors.error,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16
                                          ) : TextStyle(
                                              color: colors.success,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16
                                          ),
                                        ),
                                        Text(
                                            transactions[index].title,
                                            style: TextStyle(
                                                color: colors.text,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12
                                            )
                                        )
                                      ],
                                    ),
                                    Text(
                                      transactions[index].formatDate(),
                                      style: TextStyle(
                                          color: colors.subtext,
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 12)
                          ],
                        );
                      }
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}