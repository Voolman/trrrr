import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_7/data/models/ModelOrders.dart';
import 'package:training_final_7/data/models/ModelPoints.dart';
import 'package:training_final_7/domain/TrackPresenter.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import '../../theme/colors.dart';
import '../../widgets/dialogs.dart';
import '../SendPackagePage.dart';

class Track extends StatefulWidget {
  const Track({super.key});

  @override
  State<Track> createState() => _TrackState();
}

List<PlacemarkMapObject> mapPoints = [];
String id = '';

class _TrackState extends State<Track> {
  ModelOrder? order;
  List<ModelPoints> points = [];
  bool isFinishLoading = false;
  List dates = ['', '', '', ''];

  @override
  void initState() {
    super.initState();
    getIdOrder((String res) {
      setState(() {
        id = res;
      });
    }, (String e) {
      showError(context, e);
    });
    getPointsFunc((List<ModelPoints> res) {
      setState(() {
        points = res;
      });
    }, (String e) {
      showError(context, e);
    });
    getSubscribe((ModelOrder currentOrder, List currentDates) {
      setState(() {
        order = currentOrder;
        dates = currentDates;
      });
    });
  }

  List<PlacemarkMapObject> getPlacemarkMapObjects() {
    List<PlacemarkMapObject> pointsWidget = [];
    if (points.isEmpty) {
      return [];
    }
    for (int i = 0; i < points.length; i++) {
      var currentPoint = points[i];
      pointsWidget.add(PlacemarkMapObject(
          mapId: MapObjectId("marker-${currentPoint.id}"),
          point: Point(
              latitude: double.parse(currentPoint.geoLat!), longitude: double.parse(currentPoint.geoLong!)),
          opacity: 1,
          icon: PlacemarkIcon.single(PlacemarkIconStyle(
              image: BitmapDescriptor.fromAssetImage('assets/map_point.png'),
              ))));
    }
    print(pointsWidget);
    return pointsWidget;
  }

  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      body: (order == null)
          ? const SizedBox()
          : Column(
              children: [
                SizedBox(
                  height: 320,
                  width: double.infinity,
                  child: YandexMap(
                    mapObjects: <PlacemarkMapObject>[]+getPlacemarkMapObjects(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 42, left: 24, right: 24),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            'Tracking Number',
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge
                                ?.copyWith(fontSize: 16),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      Row(
                        children: [
                          SvgPicture.asset('assets/snow.svg'),
                          const SizedBox(width: 8),
                          Text(
                            'R-$id',
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: colors.primary),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: [
                          Text(
                            'Package Status',
                            style: TextStyle(
                                color: colors.subtext,
                                fontSize: 14,
                                fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                      const SizedBox(height: 24),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            children: [
                              SizedBox(
                                height: 21,
                                width: 21,
                                child: Checkbox(
                                  value: order!.status >= 1,
                                  activeColor: colors.primary,
                                  side: BorderSide(
                                      color: colors.primary, width: 1),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(2)),
                                  onChanged: (value) {},
                                ),
                              ),
                              Container(
                                height: 34,
                                width: 1,
                                decoration:
                                    BoxDecoration(color: colors.subtext),
                              ),
                              (order!.status > 0)
                                  ? SizedBox(
                                      height: 21,
                                      width: 21,
                                      child: Checkbox(
                                        value: order!.status >= 2,
                                        activeColor: colors.primary,
                                        side: BorderSide(
                                            color: colors.primary, width: 1),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(2)),
                                        onChanged: (value) {},
                                      ),
                                    )
                                  : Container(
                                      height: 21,
                                      width: 21,
                                      decoration: BoxDecoration(
                                          color: Color.fromARGB(
                                              255, 224, 224, 224)),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 3),
                                        child:
                                            SvgPicture.asset('assets/line.svg'),
                                      ),
                                    ),
                              Container(
                                height: 34,
                                width: 1,
                                decoration:
                                    BoxDecoration(color: colors.subtext),
                              ),
                              (order!.status > 1)
                                  ? SizedBox(
                                      height: 21,
                                      width: 21,
                                      child: Checkbox(
                                        value: order!.status >= 3,
                                        activeColor: colors.primary,
                                        side: BorderSide(
                                            color: colors.primary, width: 1),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(2)),
                                        onChanged: (value) {},
                                      ),
                                    )
                                  : Container(
                                      height: 21,
                                      width: 21,
                                      decoration: BoxDecoration(
                                          color: Color.fromARGB(
                                              255, 224, 224, 224)),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 3),
                                        child:
                                            SvgPicture.asset('assets/line.svg'),
                                      ),
                                    ),
                              Container(
                                height: 34,
                                width: 1,
                                decoration:
                                    BoxDecoration(color: colors.subtext),
                              ),
                              (order!.status > 2)
                                  ? SizedBox(
                                      height: 21,
                                      width: 21,
                                      child: Checkbox(
                                        value: order!.status >= 4,
                                        activeColor: colors.primary,
                                        side: BorderSide(
                                            color: colors.primary, width: 1),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(2)),
                                        onChanged: (value) {},
                                      ),
                                    )
                                  : Container(
                                      height: 21,
                                      width: 21,
                                      decoration: BoxDecoration(
                                          color: Color.fromARGB(
                                              255, 224, 224, 224)),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 3),
                                        child:
                                            SvgPicture.asset('assets/line.svg'),
                                      ),
                                    ),
                            ],
                          ),
                          const SizedBox(width: 7),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Courier requested',
                                style: TextStyle(
                                    color: (order!.status == 0)
                                        ? colors.primary
                                        : colors.subtext,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                dates[0],
                                style: TextStyle(
                                    color: colors.secondary,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Text(
                                'Package ready for delivery',
                                style: TextStyle(
                                    color: (order!.status == 1)
                                        ? colors.primary
                                        : colors.subtext,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                dates[1],
                                style: TextStyle(
                                    color: (order!.status > 0)
                                        ? colors.secondary
                                        : colors.subtext,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Text(
                                'Package in transit',
                                style: TextStyle(
                                    color: (order!.status == 2)
                                        ? colors.primary
                                        : colors.subtext,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                dates[2],
                                style: TextStyle(
                                    color: (order!.status > 1)
                                        ? colors.secondary
                                        : colors.subtext,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400),
                              ),
                              const SizedBox(
                                height: 13,
                              ),
                              Text(
                                'Package delivered',
                                style: TextStyle(
                                    color: (order!.status == 1)
                                        ? colors.primary
                                        : colors.subtext,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                dates[3],
                                style: TextStyle(
                                    color: (order!.status > 2)
                                        ? colors.secondary
                                        : colors.subtext,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      SizedBox(
                        height: 46,
                        width: double.infinity,
                        child: FilledButton(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      const SendPackagePage()));
                            },
                            style: Theme.of(context).filledButtonTheme.style,
                            child: Text('View Package Info',
                                style:
                                    Theme.of(context).textTheme.labelMedium)),
                      ),
                    ],
                  ),
                )
              ],
            ),
    );
  }
}
