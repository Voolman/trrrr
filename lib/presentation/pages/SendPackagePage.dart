import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_7/data/models/ModelOrders.dart';
import 'package:training_final_7/domain/SendPackagePresenter.dart';
import 'package:training_final_7/presentation/pages/TransactionPage.dart';

import '../../data/models/ModelDestination.dart';
import '../../domain/SendPackagePagePresenter.dart';
import '../theme/colors.dart';
import '../widgets/dialogs.dart';


class SendPackagePage extends StatefulWidget {
  const SendPackagePage({super.key});

  @override
  State<SendPackagePage> createState() => _SendPackagePageState();
}
Widget currentWidget = const SizedBox();
class _SendPackagePageState extends State<SendPackagePage> {
  @override
  void initState(){
    super.initState();
    getPackageInformation(
            (res, res1){
          setState(() {
            createList(res, res1);
          });
        },
            (String e){showError(context, e);}
    );
  }

  Widget createList(ModelOrder order, List<ModelDestinations> destinations){
    var colors = LightColorsApp();
    List<Widget> destinationsWidgets = [];
    for(int i = 0; i < destinations.length; i++){
      destinationsWidgets.add(
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
                '${i+1}. ${destinations[i].destinationAddress} , ${destinations[i]
                    .destinationCountry}',
                style: Theme
                    .of(context)
                    .textTheme
                    .titleSmall
            ),
            const SizedBox(height: 4,),
            Text(
                destinations[i].destinationPhone,
                style: Theme
                    .of(context)
                    .textTheme
                    .titleSmall
            ),
            const SizedBox(height: 8)
          ],
        )
      );
    }
    currentWidget = Column(
      children: [
        Container(
          height: 108,
          width: double.infinity,
          decoration: BoxDecoration(
              color: colors.background,
              boxShadow: const [
                BoxShadow(
                  color: Color.fromARGB(38, 0, 0, 0),
                  blurRadius: 5,
                  offset: Offset(0, 2),
                )
              ]
          ),
          child: Column(
            children: [
              SizedBox(
                height: 106,
                width: double.infinity,
                child: Column(
                  children: [
                    const SizedBox(height: 73,),
                    Row(
                      children: [
                        const SizedBox(width: 15),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset('assets/back.svg')
                        ),
                        const SizedBox(width: 109),
                        Text(
                          'Send a package',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleMedium
                              ?.copyWith(fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 24),
              Text(
                'Package Information',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: colors.primary
                ),
              ),
              const SizedBox(height: 8,),
              Text(
                'Origin details',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: colors.text
                ),
              ),
              const SizedBox(height: 4,),
              Text(
                  '${order.pointAddress} , ${order.pointCountry}',
                  style: Theme
                      .of(context)
                      .textTheme
                      .titleSmall
              ),
              const SizedBox(height: 4,),
              Text(
                  order.pointPhone,
                  style: Theme
                      .of(context)
                      .textTheme
                      .titleSmall
              ),
              const SizedBox(height: 8,),
              Text(
                  'Destination details',
                  style: Theme
                      .of(context)
                      .textTheme
                      .titleSmall
                      ?.copyWith(color: colors.text)
              ),
              const SizedBox(height: 4),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[] + destinationsWidgets,
              ),
              Text(
                'Other details',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: colors.text
                ),
              ),
              const SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Package Items',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      order.packageItem,
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Weight of items',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      order.packageWeight.toString(),
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Worth of Items',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      order.packageWorth.toString(),
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Tracking Number',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      'R-${order.id}',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 37,),
              Divider(height: 1, color: colors.subtext),
              const SizedBox(height: 8,),
              Text(
                'Charges',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: colors.primary
                ),
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Delivery Charges',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      'N${costCharges(destinations.length)}.00',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 8,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Instant delivery',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      'N300.00',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 8,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Tax and Service Charges',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      'N${costTax(destinations.length)}0',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 9,),
              Divider(height: 1, color: colors.subtext),
              const SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Package total',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      'N${totalCost(destinations.length).toString()}0',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 24),
              const Row(
                children: [
                  Expanded(
                    child: Text(
                      'Click on  delivered for successful delivery and rate rider or report missing item',
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          height: 4/3,
                          color: Color.fromARGB(255, 47, 128, 237)
                      ),
                    ),
                  ),
                  SizedBox(width: 10)
                ],
              ),
              const SizedBox(height: 24),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 48,
                    width: 158,
                    child: OutlinedButton(
                        onPressed: () {},
                        style: OutlinedButton.styleFrom(
                            side: BorderSide(color: colors.primary),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            )
                        ),
                        child: Text(
                          'Report',
                          style: TextStyle(
                              color: colors.primary,
                              fontSize: 16,
                              fontWeight: FontWeight.w700
                          ),
                        )
                    ),
                  ),
                  SizedBox(
                    height: 48,
                    width: 158,
                    child: FilledButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const TransactionPage()));
                        },
                        style: FilledButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)
                          ),
                          backgroundColor: colors.primary,
                        ),
                        child: Text(
                          'Successful',
                          style: TextStyle(
                              color: colors.background,
                              fontSize: 16,
                              fontWeight: FontWeight.w700
                          ),
                        )
                    ),
                  )
                ],
              )
            ],
          ),
        )
      ],
    );
    return currentWidget;
  }


  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
        backgroundColor: colors.background,
        resizeToAvoidBottomInset: false,
        body: currentWidget
    );
  }
}