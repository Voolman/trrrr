import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_7/domain/TransactionPresenter.dart';
import '../../domain/accelerometr.dart';
import '../../main.dart';
import '../widgets/dialogs.dart';
import 'Home.dart';

class TransactionPage extends StatefulWidget {
  const TransactionPage({super.key});

  @override
  State<TransactionPage> createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage> {
  var rate = 0;
  TextEditingController controller = TextEditingController();
  double x = 0, y = 0, z = 0;
  String? error;
  bool isLoading = true;
  double currentNum = 0;

  AccelerometerUseCase useCase = AccelerometerUseCase();

  @override
  void initState(){
    super.initState();
    timerTransaction(
            (){
          setState(() {});
        }
    );
    useCase.launchListenSensor(
        onChange: (event) {
          setState(() {
            if (isLoading) {
              isLoading = false;
            }
            if (currentNum + event.x >= 7.51){
              currentNum = -9.81;
              (rate == 0) ?  null : rate -= 1;
            }
            if (currentNum + event.x <= -7.51){
              currentNum = 9.81;
              (rate == 5) ?  null : rate += 1;
            }
            if (event.x == 0){
              currentNum = 0;
            }
          });
        },
        onError: (error) {
          setState(() {
            this.error = error.toString();
          });
        }
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 144),
              child: (getTime() == 0) ? Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset('assets/good.svg'),
                  const SizedBox(height: 75,),
                  Text(
                    'Transaction Successful',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const SizedBox(height: 8),
                  Text(
                    'Your Item has been delivered successfully',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: colors.text,
                        height: 8/7
                    ),
                  ),
                  const SizedBox(height: 67)
                ],
              ) : Column(
                children: [
                  SizedBox(
                    height: 119,
                    width: 119,
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.transparent,
                      valueColor: AlwaysStoppedAnimation<Color>(colors.secondary),
                      strokeWidth: 13.88,
                      strokeCap: StrokeCap.round,
                    ),
                  ),
                  const SizedBox(height: 219),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 24, right: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Rate Rider',
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: colors.primary,
                      height: 8/7
                    ),
                  ),
                  const SizedBox(height: 16),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            rate = 1;
                          });
                        },
                          child: SvgPicture.asset('assets/star.svg', color: (rate > 0) ? colors.secondary : colors.subtext)
                      ),
                      const SizedBox(width: 16),
                      GestureDetector(
                          onTap: (){
                            setState(() {
                              rate = 2;
                            });
                          },
                          child: SvgPicture.asset('assets/star.svg', color: (rate > 1) ? colors.secondary : colors.subtext)
                      ),
                      const SizedBox(width: 16),
                      GestureDetector(
                          onTap: (){
                            setState(() {
                              rate = 3;
                            });
                          },
                          child: SvgPicture.asset('assets/star.svg', color: (rate > 2) ? colors.secondary : colors.subtext)
                      ),
                      const SizedBox(width: 16),
                      GestureDetector(
                          onTap: (){
                            setState(() {
                              rate = 4;
                            });
                          },
                          child: SvgPicture.asset('assets/star.svg', color: (rate > 3) ? colors.secondary : colors.subtext)
                      ),
                      const SizedBox(width: 16),
                      GestureDetector(
                          onTap: (){
                            setState(() {
                              rate = 5;
                            });
                          },
                          child: SvgPicture.asset('assets/star.svg', color: (rate > 4) ? colors.secondary : colors.subtext)
                      ),
                    ],
                  ),
                  const SizedBox(height: 37),
                  Container(
                    height: 50,
                    width: double.infinity,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromARGB(38, 0, 0, 0),
                            blurRadius: 2,
                            offset: Offset(0, 2),
                          )
                        ]
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 17, horizontal: 12),
                          child: SvgPicture.asset('assets/feedback.svg'),
                        ),
                        Expanded(
                          child: TextField(
                            controller: controller,
                            style: const TextStyle(
                                color: Color.fromARGB(255, 58, 58, 58),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                            ),
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(10),
                            ],
                            decoration: InputDecoration(
                                enabledBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.transparent)
                                ),
                                focusedBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.transparent)
                                ),
                                contentPadding: const EdgeInsets.only(left: 8, bottom: 15),
                                hintText: 'Add feedback',
                                hintStyle:  TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: colors.subtext
                                )
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 76),
                  SizedBox(
                    height: 46,
                    width: double.infinity,
                    child: FilledButton(
                        onPressed: (){
                          pressSendRate(
                              rate,
                              controller.text,
                              (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home()));},
                              (String e){showError(context, e);}
                          );
                        },
                        style: Theme.of(context).filledButtonTheme.style,
                        child: Text(
                            'Done',
                            style: Theme.of(context).textTheme.labelMedium
                        )
                    ),
                  ),
                ],
              ),
            )

          ],
        ),
      ),
    );
  }
}
