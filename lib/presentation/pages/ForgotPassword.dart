import 'package:flutter/material.dart';
import 'package:training_final_7/domain/utils.dart';
import '../../domain/ForgotPasswordPresenter.dart';
import '../theme/colors.dart';
import '../widgets/CustomTextField.dart';
import '../widgets/dialogs.dart';
import 'LogIn.dart';
import 'OTPVerification.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({super.key});


  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}
bool isChecked = false;
class _ForgotPasswordState extends State<ForgotPassword> {
  bool enableButton = false;
  TextEditingController email = TextEditingController();
  void onChanged(_){
    setState(() {
      enableButton = isValid(email.text);
    });
  }
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 155, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'Forgot Password',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Enter your email address',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              const SizedBox(height: 56),
              CustomTextField(
                  label: 'Email Address',
                  hint: '***********@mail.com',
                  onChanged: onChanged,
                  controller: email,
                  isError: isValidEmail(email.text),
              ),
              const SizedBox(height: 56),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (enableButton) ? (){
                        showLoading(context);
                        pressSendOTP(
                            email.text,
                                (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => OTPVerification(email: email.text)));},
                                (String e){showError(context, e);}

                        );
                        Navigator.of(context).pop();
                      } : null,
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Send OTP',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  )
              ),
              const SizedBox(height: 20),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));
                },
                child: RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Remember password? Back to ',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                          ),
                          TextSpan(
                              text: 'Sign in',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.primary)
                          )
                        ]
                    )
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}