import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_7/domain/HomePresenter.dart';
import 'package:training_final_7/presentation/pages/tabs/HomePage.dart';
import 'package:training_final_7/presentation/pages/tabs/Profile.dart';
import 'package:training_final_7/presentation/pages/tabs/Track.dart';
import 'package:training_final_7/presentation/pages/tabs/Wallet.dart';
import 'package:training_final_7/presentation/widgets/dialogs.dart';
import '../../main.dart';

class Home extends StatefulWidget {
  final int currentIndex;
  const Home({super.key, this.currentIndex = 0});


  @override
  State<Home> createState() => _HomeState();
}

int currentIndex = 0;
bool isCan = true;

class _HomeState extends State<Home> {
  @override
  void initState(){
    super.initState();
    currentIndex = widget.currentIndex;
    getCheck(
        (res){
          setState(() {
            isCan = res;
          });
        },
        (String e){showError(context, e);}
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
        backgroundColor: colors.background,
        bottomNavigationBar: BottomNavigationBar(
          elevation: 0,
          currentIndex: currentIndex,
          type: BottomNavigationBarType.fixed,
          backgroundColor: colors.background,
          selectedItemColor: colors.primary,
          unselectedItemColor: colors.subtext,
          unselectedLabelStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 12,
            color: colors.subtext
          ),
          selectedLabelStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 12,
            color: colors.primary
          ),
          onTap: (newIndex){
            (newIndex == 2 && isCan) ? setState(() {
              currentIndex = newIndex;
            }) : (newIndex != 2) ? setState(() {
              currentIndex = newIndex;
          }) : null;},
            items: [
            BottomNavigationBarItem(icon: SvgPicture.asset('assets/house.svg',
                color: (currentIndex == 0) ? colors.primary : colors.subtext),
                label: 'Home'),
            BottomNavigationBarItem(icon: SvgPicture.asset('assets/wallet.svg',
                color: (currentIndex == 1) ? colors.primary : colors.subtext),
                label: 'Wallet'),
            BottomNavigationBarItem(icon: SvgPicture.asset('assets/car.svg',
                color: (currentIndex == 2) ? colors.primary : colors.subtext),
                label: 'Track'),
            BottomNavigationBarItem(icon: SvgPicture.asset('assets/profile.svg',
                color: (currentIndex == 3) ? colors.primary : colors.subtext),
                label: 'Profile')
          ],
        ),
        body: [const HomePage(), const Wallet(), const Track(), const Profile()][currentIndex]
    );
  }
}