import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../main.dart';

class AddPayment extends StatefulWidget {
  const AddPayment({super.key});


  @override
  State<AddPayment> createState() => _AddPaymentState();
}
var selectedValue = 1;
var selectedValue2 = 1;
class _AddPaymentState extends State<AddPayment> {
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Column(
        children: [
          Container(
            width: double.infinity,
            height: 108,
            decoration: BoxDecoration(color: colors.background, boxShadow: const [
              BoxShadow(
                  color: Color.fromARGB(38, 0, 0, 0),
                  offset: Offset(0, 2),
                  blurRadius: 5)
            ]),
            child: Padding(
              padding: const EdgeInsets.only(top: 69, left: 15),
              child: Row(
                children: [
                  GestureDetector(
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                      child: SvgPicture.asset('assets/back.svg')),
                  const SizedBox(width: 79,),
                  Text(
                    'Add Payment method',
                    style: Theme.of(context).textTheme.labelSmall,
                  )
                ],
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 67, left: 25, right: 24),
            child: Column(
              children: [
                Container(
                  height: 84,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: colors.background,
                    boxShadow: const [
                      BoxShadow(
                          color: Color.fromARGB(38, 0, 0, 0),
                          offset: Offset(0, 2),
                          blurRadius: 5)
                    ]
                  ),
                  child: RadioListTile(
                    contentPadding: const EdgeInsets.symmetric(vertical: 7),
                    activeColor: colors.primary,
                    title: Text(
                        'Pay with wallet',
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        height: 5/4,
                        color: colors.text
                      ),
                    ),
                      subtitle: Text(
                        'complete the payment using your e wallet',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            height: 4/3,
                            color: colors.subtext
                        ),
                      ),
                      value: 1,
                      groupValue: selectedValue,
                      onChanged: (value){
                        setState(() {
                          selectedValue = value!;
                        });
                      }
                  ),
                ),
                const SizedBox(height: 12),
                Container(
                  height: (selectedValue != 2) ? 84 : 256,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: colors.background,
                      boxShadow: const [
                        BoxShadow(
                            color: Color.fromARGB(38, 0, 0, 0),
                            offset: Offset(0, 2),
                            blurRadius: 5)
                      ]
                  ),
                  child: Column(
                    children: [
                      RadioListTile(
                          contentPadding: const EdgeInsets.only(top: 7),
                          activeColor: colors.primary,
                          title: Text(
                            'Credit / debit card',
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                height: 5/4,
                                color: colors.text
                            ),
                          ),
                          subtitle: Text(
                            'complete the payment using your debit card',
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                height: 4/3,
                                color: colors.subtext
                            ),
                          ),
                          value: 2,
                          groupValue: selectedValue,
                          onChanged: (value){
                            setState(() {
                              selectedValue = value!;
                            });
                          }
                      ),
                      (selectedValue == 2) ? Padding(
                        padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
                        child: Column(
                          children: [
                            Container(
                              height: 68,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: colors.background,
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Color.fromARGB(38, 0, 0, 0),
                                        offset: Offset(0, 2),
                                        blurRadius: 5)
                                  ]
                              ),
                              child: RadioListTile(
                                  contentPadding: const EdgeInsets.only(top: 6, right: 8),
                                  activeColor: colors.primary,
                                  title: Text(
                                    '**** **** 3323',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16,
                                        height: 5/4,
                                        color: colors.text
                                    ),
                                  ),
                                  secondary: SvgPicture.asset('assets/trash.svg', fit: BoxFit.scaleDown,),
                                  value: 1,
                                  groupValue: selectedValue2,
                                  onChanged: (value){
                                    setState(() {
                                      selectedValue2 = value!;
                                    });
                                  }
                              ),
                            ),
                            const SizedBox(height: 12),
                            Container(
                              height: 68,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: colors.background,
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Color.fromARGB(38, 0, 0, 0),
                                        offset: Offset(0, 2),
                                        blurRadius: 5)
                                  ]
                              ),
                              child: RadioListTile(
                                  contentPadding: const EdgeInsets.only(top: 6, right: 8),
                                  activeColor: colors.primary,
                                  title: Text(
                                    '**** **** 1547',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16,
                                        height: 5/4,
                                        color: colors.text
                                    ),
                                  ),
                                  secondary: SvgPicture.asset('assets/trash.svg', fit: BoxFit.scaleDown,),
                                  value: 2,
                                  groupValue: selectedValue2,
                                  onChanged: (value){
                                    setState(() {
                                      selectedValue2 = value!;
                                    });
                                  }
                              ),
                            ),
                          ],
                        ),
                      ) : const SizedBox()
                    ],
                  ),
                ),
                SizedBox(height: (selectedValue == 2) ? 166 : 338),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){},
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Proceed to pay',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}