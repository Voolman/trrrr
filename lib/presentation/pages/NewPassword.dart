import 'package:flutter/material.dart';
import 'package:training_final_7/domain/utils.dart';
import '../../domain/NewPasswordPresenter.dart';
import '../widgets/CustomTextField.dart';
import '../widgets/dialogs.dart';
import 'LogIn.dart';

class NewPassword extends StatefulWidget {
  const NewPassword({super.key});


  @override
  State<NewPassword> createState() => _NewPasswordState();
}
bool isChecked = false;
class _NewPasswordState extends State<NewPassword> {
  TextEditingController confirmPassword = TextEditingController();
  TextEditingController password = TextEditingController();
  bool enableButton = false;
  void onChanged(_){
    setState(() {
      enableButton = isValidNewPassword(password.text, confirmPassword.text);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 155, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'New Password',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Enter new password',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              const SizedBox(height: 46),
              CustomTextField(
                  label: 'Password',
                  hint: '**********',
                  enableObscure: true,
                  onChanged: onChanged,
                  controller: password,
                  isError: isValidFirstPassword(password.text),
              ),
              CustomTextField(
                  label: 'Confirm Password',
                  hint: '**********',
                  enableObscure: true,
                  onChanged: onChanged,
                  controller: confirmPassword,
                  isError: isValidNewPassword(password.text, confirmPassword.text),
              ),
              const SizedBox(height: 71),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (enableButton) ? (){
                        showLoading(context);
                        pressChangePassword(
                            password.text,
                                (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));},
                                (String e){showError(context, e);}

                        );
                        Navigator.of(context).pop();
                      } : null,
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Log in',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}