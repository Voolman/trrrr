import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../theme/colors.dart';


class CustomTextField extends StatefulWidget {
  final String label;
  final String hint;
  final Function(String) onChanged;
  final TextEditingController controller;
  final bool enableObscure;
  final bool isNumbers;
  final bool isError;
  const CustomTextField({super.key, required this.label, required this.hint, required this.onChanged, required this.controller, this.enableObscure = false, required this.isError, this.isNumbers = false});


  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}
bool isObscure = true;
class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Column(
        children: [
          SizedBox(height: 24,),
          Row(
            children: [
              Text(
                widget.label,
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ],
          ),
          const SizedBox(height: 8),
          SizedBox(
            height: 44,
            child: TextField(
              controller: widget.controller,
              onChanged: widget.onChanged,
              keyboardType: (widget.isNumbers) ? TextInputType.phone : TextInputType.text,
              obscuringCharacter: '*',
              obscureText: (widget.enableObscure) ? isObscure : false,
              decoration: InputDecoration(
                enabledBorder: (widget.isError) ? Theme.of(context).inputDecorationTheme.enabledBorder : Theme.of(context).inputDecorationTheme.errorBorder,
                focusedBorder: (widget.isError) ? Theme.of(context).inputDecorationTheme.focusedBorder : Theme.of(context).inputDecorationTheme.errorBorder,
                contentPadding: const EdgeInsets.symmetric(horizontal: 14, vertical: 10),
                hintText: widget.hint,
                hintStyle: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.hint),
                suffixIcon: (widget.enableObscure) ? GestureDetector(
                  onTap: (){
                    setState(() {
                      isObscure = !isObscure;
                    });
                  },
                  child: SvgPicture.asset(
                      'assets/eye-slash.svg',
                      fit: BoxFit.scaleDown,
                      colorFilter: ColorFilter.mode(colors.iconTint, BlendMode.dst)),
                ) : null
              ),
            )
          )
        ],
    );
  }
}