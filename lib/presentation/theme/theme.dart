import 'package:flutter/material.dart';
import 'colors.dart';

var lightColors = LightColorsApp();
var lightTheme = ThemeData(
    textTheme: TextTheme(
        titleLarge: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 24,
            color: lightColors.text,
            height: 15/12
        ),
        titleMedium: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: lightColors.subtext
        ),
        titleSmall: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: lightColors.subtext
        ),
        labelMedium: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 16,
            color: lightColors.background
        ),
      labelSmall: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 16,
        height: 1,
        color: lightColors.subtext
      )
    ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
            ),
            backgroundColor: lightColors.primary,
            disabledBackgroundColor: lightColors.subtext
        )
    ),
    inputDecorationTheme: InputDecorationTheme(
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: lightColors.subtext, width: 1),
            borderRadius: BorderRadius.circular(4)
        ),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: lightColors.subtext, width: 1),
            borderRadius: BorderRadius.circular(4)
        ),
      errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: lightColors.error, width: 1),
          borderRadius: BorderRadius.circular(4)
      ),
    )
);

var darkColors = DarkColorsApp();
var darkTheme = ThemeData(
    textTheme: TextTheme(
        titleLarge: TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 24,
          color: darkColors.text,
          height: 15/12
        ),
        titleMedium: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: darkColors.subtext
        ),
        titleSmall: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: darkColors.subtext
        ),
        labelMedium: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 16,
            color: darkColors.background,
            height: 1
        ),
        labelSmall: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 16,
            height: 1,
            color: darkColors.subtext
        )
    ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
            ),
            backgroundColor: darkColors.primary,
            disabledBackgroundColor: darkColors.subtext
        )
    ),
    inputDecorationTheme: InputDecorationTheme(
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: darkColors.subtext, width: 1),
            borderRadius: BorderRadius.circular(4)
        ),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: darkColors.subtext, width: 1),
            borderRadius: BorderRadius.circular(4)
        ),
      errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: darkColors.error, width: 1),
          borderRadius: BorderRadius.circular(4)
      ),
    )
);