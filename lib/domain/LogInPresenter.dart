import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_7/data/repository/cache.dart';
import 'package:training_final_7/domain/utils.dart';

import '../data/repository/supabase.dart';

Future<void> pressSignIn(String email, String password, Function onResponse, Function(String) onError) async {
  try{
    AuthResponse res = await signIn(email, password);
    onResponse(res);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}


bool isValidLogIn(String email, String password){
  return  checkEmail(email) && password.isNotEmpty;
}

Future<void> saver(String id, String email, String password) async {
  await saveAuthData(id, email, password);
}