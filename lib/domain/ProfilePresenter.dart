import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/models/ModelProfile.dart';
import '../data/repository/supabase.dart';

Future<void> pressSignOut(Function onResponse, Function(String) onError) async {
  try{
    await signOut();
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}

Future<void> getProfile(Function onResponse, Function(String) onError) async {
  try{
    var profiles = await getUserData();
    var currentId = getCurrentUserId();
    ModelProfile currentUser = profiles.where((element) => element.idUser == currentId).single;
    onResponse(currentUser);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}