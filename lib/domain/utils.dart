bool checkEmail(String email){
  return RegExp(r"^[0-9a-z]+@[0-9a-z]+\.\w{2,}$").hasMatch(email);
}

bool isValidFirstPassword(String password){
  return password.length > 5 || password.isEmpty;
}

bool isValidEmail(String email){
  return checkEmail(email) || email.isEmpty;
}
