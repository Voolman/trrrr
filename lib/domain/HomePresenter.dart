import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_7/data/repository/supabase.dart';

Future<void> getCheck(Function onResponse, Function(String) onError) async {
  try{
    var res = await checkPackage();
    onResponse(res);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}