import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import '../data/repository/supabase.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart' as yandex;


String? getUserIdFunc(){
  return getCurrentUserId();
}

Future<void> pressSendPackage(
    String originAddress,
    String originCountry,
    String originPhone,
    String othersOrder,
    String items,
    String weight,
    String worth,
    int charges,
    int instant,
    List<TextEditingController> destinationAddress,
    List<TextEditingController> destinationCountry,
    List<TextEditingController> destinationPhone,
    List<TextEditingController> destinationOthers,
    Function onResponse,
    Function(String) onError) async {
  try {
    await saveOrigin(originAddress, originCountry, originPhone, othersOrder, items, weight, worth, charges, instant);
    await saveDestinations(destinationAddress, destinationCountry, destinationPhone, destinationOthers);
    onResponse();
  } on AuthException catch (e) {
    onError(e.message);
  } on PostgrestException catch (e) {
    onError(e.toString());
  } on Exception catch (e) {
    onError(e.toString());
  }
}

int costCharges(int num){
  return 2500*num;
}
double totalCost(int num){
  return costCharges(num) + costTax(num) + 300;
}

double costTax(int num){
  return (2500*num +300)*0.05;
}

// Future<void> geoPoint(yandex.Point point, Function(String) onResponse, Function(String) onError) async {
//   var response = await YandexSearch.sea
// }



