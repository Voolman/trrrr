import 'package:supabase_flutter/supabase_flutter.dart';
import '../data/repository/supabase.dart';

Future<void> getPackageInformation(Function onResponse, Function(String) onError) async {
  try{
    var currentOrder = await getOrderInformation();
    var currentDestinations = await getDestinationInformation();
    onResponse(currentOrder, currentDestinations);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e) {
    onError(e.toString());
  }
}
