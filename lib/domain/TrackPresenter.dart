import 'package:intl/intl.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_7/data/models/ModelOrders.dart';
import 'package:training_final_7/data/repository/supabase.dart';

Future<void> getIdOrder(Function onResponse, Function(String) onError) async {
  try{
    var res = await getOrderId();
    onResponse(res);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e) {
    onError(e.toString());
  }
}

void getSubscribe(Function onResponse){
  ModelOrder? order;
  List<String> dates = ['', '', '', ''];
  getOrderInformation().then((value) {
    order = value;
    subscribeOrder(order!.id, (newOrder) {
        order = ModelOrder.fromJson(newOrder);
        for (int i = 0; i <= 3; i++) {
            dates[i] = DateFormat('MMMM d yyyy h:mma').format(DateTime.now()).toString();
        }
        onResponse(order, dates);
    });
    onResponse(order, dates);
  });
}

Future<void> getPointsFunc(Function onResponse, Function(String) onError) async {
  try{
    var res = await getPoints();
    onResponse(res);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e) {
    onError(e.toString());
  }
}