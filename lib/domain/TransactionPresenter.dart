import 'dart:async';

import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_7/data/repository/supabase.dart';

import '../data/storage/seconds.dart';

void timeTransaction(){
  if (seconds != 0){
    seconds --;
  }
}

void timerTransaction(Function callback){
  Timer(
      const Duration(seconds: 1), (){
    timeTransaction();
    callback();
    timerTransaction(callback);
  });
}

int getTime(){
  return seconds;
}

Future<void> pressSendRate(int rate, String feedback, Function onResponse, Function(String) onError) async {
  try{
    await pushRate(rate, feedback);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}