import 'package:supabase_flutter/supabase_flutter.dart';
import '../data/repository/supabase.dart';

Future<void> pressChangePassword(String password, Function onResponse, Function(String) onError) async {
  try{
    await changePassword(password);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}

bool isValidNewPassword(String password, String confirmPassword){
  return password == confirmPassword;
}