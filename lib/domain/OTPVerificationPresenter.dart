import 'dart:async';
import 'package:supabase_flutter/supabase_flutter.dart';
import '../data/repository/supabase.dart';
import '../data/storage/seconds.dart';

void time(){
  if (lostSeconds != 0){
    lostSeconds --;
  }
}

void timer(Function callback){
  Timer(
      const Duration(seconds: 1), (){
    time();
    callback();
    timer(callback);
  });
}

int getSeconds(){
  return lostSeconds;
}

void refresh(int num){
  lostSeconds = num;
}

bool isValidOTP(String code){
  return code.length == 6;
}


Future<void> pressVerifyOTP(String email, String code, Function onResponse, Function(String) onError) async {
  try{
    await verifyOTP(email, code);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}
