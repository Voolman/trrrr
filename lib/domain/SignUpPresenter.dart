import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_7/domain/utils.dart';
import '../data/repository/supabase.dart';

Future<void> pressSignUp(String email, String phone, String fullName, String password, Function onResponse, Function(String) onError) async {
  try{
    var res = await signUp(email, password);
    await dataSignUp(res.user!.id, phone, fullName);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}

bool isValidPassword(String password, String confirmPassword){
  return password == confirmPassword;
}

bool isValid(String name, String phone, String email, String password, String confirmPassword, bool isConfirm){
  return name.isNotEmpty && phone.isNotEmpty && checkEmail(email) && password.isNotEmpty && confirmPassword.isNotEmpty && password == confirmPassword && isConfirm;
}